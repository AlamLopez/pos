<?php

	require_once "conexion.php";

	/**
	 * Clase que modela Usuarios
	 * 
	 * @package modelos
	 * @author Alam Lopez <alam.lqnpa6@hotmail.com>
	 */
	class ModeloUsuarios
	{
		
		/**
		 * Función de clase que muestra los usuarios de la base de datos
		 * 
		 * @param $tabla, tabla de la base de datos
		 * @param $item, columna de la base de datos
		 * @param $valor, valor almacenado en la base de datos
		 * @return $stmt, resultado de la consulta a la base de datos
		 */
		static public function mdlMostrarUsuarios($tabla, $item, $valor)
		{

			if($item != null){

				// Consulta a la base de datos
				$stmt = Conexion::conectar() -> prepare("SELECT * FROM $tabla WHERE $item = :$item");

				// Se asigna valor a los parametros de consulta
				$stmt -> bindParam(":" . $item, $valor, PDO::PARAM_STR);

				// Se ejecuta la consulta
				$stmt -> execute();

				// Retorna el resultado de la consulta en un array (1 fila)
				return $stmt -> fetch();
			}else{
				// Consulta a la base de datos
				$stmt = Conexion::conectar() -> prepare("SELECT * FROM $tabla");

				// Se ejecuta la consulta
				$stmt -> execute();

				// Retorna el resultado de la consulta en un array (1 fila)
				return $stmt -> fetchAll();	
			}


			// Se cierra la conexión con la base de datos
			$stmt -> close();
			$stmt = null;
			
		}

		/**
		 * Función de clase que inserta usuarios en la base de datos
		 * 
		 * @param $tabla, tabla de la base de datos
		 * @param $datos, datos que se insertarán en la base de datos
		 */
		static public function mdlIngresarUsuario($tabla, $datos)
		{
			// Consulta a la base de datos
			$stmt = Conexion::conectar() -> prepare("INSERT INTO $tabla(nombre, usuario, password, perfil, foto) VALUES (:nombre, :usuario, :password, :perfil, :foto)");

			// Se asignan valores a los parametros de consulta
			$stmt -> bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
			$stmt -> bindParam(":usuario", $datos["usuario"], PDO::PARAM_STR);
			$stmt -> bindParam(":password", $datos["password"], PDO::PARAM_STR);
			$stmt -> bindParam(":perfil", $datos["perfil"], PDO::PARAM_STR);
			$stmt -> bindParam(":foto", $datos["foto"], PDO::PARAM_STR);

			// Se ejecuta la consulta
			if($stmt -> execute()){
				return "ok";
			}else{
				return "error";
			}

			// Se cierra la conexión con la base de datos
			$stmt -> close();
			$stmt = null;
		}


		/**
		 * Función de clase que edita usuarios en la base de datos
		 * 
		 * @param $tabla, tabla de la base de datos
		 * @param $datos, datos que se editarán en la base de datos
		 */
		static public function mdlEditarUsuario($tabla, $datos){
			
			// Consulta a la base de datos
			$stmt = Conexion::conectar() -> prepare("UPDATE $tabla SET nombre = :nombre, password = :password, perfil = :perfil, foto = :foto WHERE usuario = :usuario");

			// Se asignan valores a los parametros de consulta
			$stmt -> bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
			$stmt -> bindParam(":password", $datos["password"], PDO::PARAM_STR);
			$stmt -> bindParam(":perfil", $datos["perfil"], PDO::PARAM_STR);
			$stmt -> bindParam(":foto", $datos["foto"], PDO::PARAM_STR);
			$stmt -> bindParam(":usuario", $datos["usuario"], PDO::PARAM_STR);

			// Se ejecuta la consulta
			if($stmt -> execute()){
				return "ok";
			}else{
				return "error";
			}

			// Se cierra la conexión con la base de datos
			$stmt -> close();
			$stmt = null;
		}

		/**
		 * Función de clase que edita usuarios en la base de datos
		 * 
		 * @param $tabla, tabla de la base de datos
		 * @param $item1, campo que se actualizará en la base de datos
		 * @param $valor1, valor que se le asignará a $item1 al actualizar
		 * @param $item2, campo que servirá de condición en al actualizar 
		 * @param $valor2, valor que se le asignará a $item2 al actualizar
		 */
		static public function mdlActualizarUsuario($tabla, $item1, $valor1, $item2, $valor2){

			// Consulta a la base de datos
			$stmt = Conexion::conectar() -> prepare("UPDATE $tabla SET $item1 = :$item1 WHERE $item2 = :$item2");

			// Se asignan valores a los parametros de consulta
			$stmt -> bindParam(":" . $item1, $valor1, PDO::PARAM_STR);
			$stmt -> bindParam(":" . $item2, $valor2, PDO::PARAM_STR);

			// Se ejecuta la consulta
			if($stmt -> execute()){
				return "ok";
			}else{
				return "error";
			}

			// Se cierra la conexión con la base de datos
			$stmt -> close();
			$stmt = null;

		}

		/**
		 * Función de clase que edita usuarios en la base de datos
		 * 
		 * @param $tabla, tabla de la base de datos
		 * @param $datos, campo que indicara el registro a borrar de la base de datos
		 */
		static public function mdlBorrarUsuario($tabla, $datos){

			// Consulta a la base de datos
			$stmt = Conexion::conectar() -> prepare("DELETE FROM $tabla WHERE id = :id");

			// Se asignan valores a los parametros de consulta
			$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

			// Se ejecuta la consulta
			if($stmt -> execute()){
				return "ok";
			}else{
				return "error";
			}

			// Se cierra la conexión con la base de datos
			$stmt -> close();
			$stmt = null;

		}
	}

?>