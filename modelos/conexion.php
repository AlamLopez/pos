<?php

	/**
	 * Clase que gestiona la conexión a la base de datos
	 * 
	 * @package modelos
	 * @author Alam Lopez <alam.lqnpa6@hotmail.com>
	 */
	class Conexion
	{

		/**
		 * Función que ejecuta la conexion con la base de datos
		 * 
		 * @return $link, estado de la conexión
		 */
		static public function conectar()
		{
			// Se declaran las propiedades de la conexión
			$link = new PDO("mysql:host=localhost;dbname=pos", "root", "");

			// Se ejecuta la conexion a la base de datos
			$link -> exec("set name utf8");

			// Retorna el estado de la conexión
			return $link;
		}
	}


?>