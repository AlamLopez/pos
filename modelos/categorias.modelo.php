<?php

    require_once "conexion.php";

    /**
	 * Clase que modela Categorías
	 * 
	 * @package modelos
	 * @author Alam Lopez <alam.lqnpa6@hotmail.com>
	 */
    class ModeloCategorias
    {

        /**
		 * Función de clase que muestra las categorías de la base de datos
		 * 
		 * @param $tabla, tabla de la base de datos
		 * @param $item, columna de la base de datos
		 * @param $valor, valor almacenado en la base de datos
		 * @return $stmt, resultado de la consulta a la base de datos
		 */
		static public function mdlMostrarCategorias($tabla, $item, $valor)
		{

			if($item != null){

				// Consulta a la base de datos
				$stmt = Conexion::conectar() -> prepare("SELECT * FROM $tabla WHERE $item = :$item");

				// Se asigna valor a los parametros de consulta
				$stmt -> bindParam(":" . $item, $valor, PDO::PARAM_STR);

				// Se ejecuta la consulta
				$stmt -> execute();

				// Retorna el resultado de la consulta en un array (1 fila)
				return $stmt -> fetch();
			}else{
				// Consulta a la base de datos
				$stmt = Conexion::conectar() -> prepare("SELECT * FROM $tabla");

				// Se ejecuta la consulta
				$stmt -> execute();

				// Retorna el resultado de la consulta en un array (1 fila)
				return $stmt -> fetchAll();	
			}


			// Se cierra la conexión con la base de datos
			$stmt -> close();
			$stmt = null;
			
		}

        /**
		 * Función de clase que inserta categorías en la base de datos
		 * 
		 * @param $tabla, tabla de la base de datos
		 * @param $datos, datos que se insertarán en la base de datos
		 */
        static public function mdlIngresarCategoria($tabla, $datos){

            // Consulta a la base de datos
            $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(categoria) VALUES(:categoria)");

            // Se asignan valores a los parametros de consulta
            $stmt->bindParam(":categoria", $datos, PDO::PARAM_STR);

            // Se ejecuta la consulta
            if($stmt->execute()){
                return "ok";
            }else{
                return "error";
            }

            // Se cierra la conexión con la base de datos
            $stmt->close();
            $stmt = null;
		}
		
		/**
		 * Función de clase que edita categorías en la base de datos
		 * 
		 * @param $tabla, tabla de la base de datos
		 * @param $datos, datos que se insertarán en la base de datos
		 */
        static public function mdlEditarCategoria($tabla, $datos){

            // Consulta a la base de datos
            $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET categoria = :categoria WHERE id = :id");

			// Se asignan valores a los parametros de consulta
			$stmt->bindParam(":categoria", $datos["categoria"], PDO::PARAM_STR);
			$stmt->bindParam(":id", $datos["id"], PDO::PARAM_STR);

            // Se ejecuta la consulta
            if($stmt->execute()){
                return "ok";
            }else{
                return "error";
            }

            // Se cierra la conexión con la base de datos
            $stmt->close();
            $stmt = null;
		}
		
		/**
		 * Función de clase que elimina usuarios en la base de datos
		 * 
		 * @param $tabla, tabla de la base de datos
		 * @param $datos, campo que indicara el registro a borrar de la base de datos
		 */
		static public function mdlBorrarCategoria($tabla, $datos){

			// Consulta a la base de datos
			$stmt = Conexion::conectar() -> prepare("DELETE FROM $tabla WHERE id = :id");

			// Se asignan valores a los parametros de consulta
			$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

			// Se ejecuta la consulta
			if($stmt -> execute()){
				return "ok";
			}else{
				return "error";
			}

			// Se cierra la conexión con la base de datos
			$stmt -> close();
			$stmt = null;

		}


    }

?>