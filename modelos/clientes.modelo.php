<?php

    require_once "conexion.php";

    /**
     * Clase que modela Clientes
     * 
     * @package modelos
     * @author Alam Lopez <alam.lqnpa6@hotmail.com>
     */
    class ModeloClientes
    {

        /**
         * Función de clase que muestra las categorías de la base de datos
         * 
         * @param $tabla, tabla de la base de datos
         * @param $item, columna de la base de datos
         * @param $valor, valor almacenado en la base de datos
         * @return $stmt, resultado de la consulta a la base de datos
         */
        static public function mdlMostrarClientes($tabla, $item, $valor)
        {
            if($item != null){

                $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
    
                $stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
    
                $stmt -> execute();
    
                return $stmt -> fetch();
    
            }else{
    
                $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");
    
                $stmt -> execute();
    
                return $stmt -> fetchAll();
    
            }
    
            $stmt -> close();
    
            $stmt = null;
        }

        /**
		 * Función de clase que inserta categorías en la base de datos
		 * 
		 * @param $tabla, tabla de la base de datos
		 * @param $datos, datos que se insertarán en la base de datos
		 */
        static public function mdlIngresarCliente($tabla, $datos)
        {

            $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(nombre, documento, email, telefono, direccion, fecha_nacimiento) VALUES (:nombre, :documento, :email, :telefono, :direccion, :fecha_nacimiento)");

            $stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
            $stmt->bindParam(":documento", $datos["documento"], PDO::PARAM_INT);
            $stmt->bindParam(":email", $datos["email"], PDO::PARAM_STR);
            $stmt->bindParam(":telefono", $datos["telefono"], PDO::PARAM_STR);
            $stmt->bindParam(":direccion", $datos["direccion"], PDO::PARAM_STR);
            $stmt->bindParam(":fecha_nacimiento", $datos["fecha_nacimiento"], PDO::PARAM_STR);

            if($stmt->execute()){

                return "ok";

            }else{

                return "error";
            
            }

            $stmt->close();
            $stmt = null;

        }

        /**
		 * Función de clase que edita clientes en la base de datos
		 * 
		 * @param $tabla, tabla de la base de datos
		 * @param $datos, datos que se insertarán en la base de datos
		 */
        static public function mdlEditarCliente($tabla, $datos)
        {

            $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombre = :nombre, documento = :documento, email = :email, telefono = :telefono, direccion = :direccion, fecha_nacimiento = :fecha_nacimiento WHERE id = :id");

            $stmt->bindParam(":id", $datos["id"], PDO::PARAM_INT);
            $stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
            $stmt->bindParam(":documento", $datos["documento"], PDO::PARAM_INT);
            $stmt->bindParam(":email", $datos["email"], PDO::PARAM_STR);
            $stmt->bindParam(":telefono", $datos["telefono"], PDO::PARAM_STR);
            $stmt->bindParam(":direccion", $datos["direccion"], PDO::PARAM_STR);
            $stmt->bindParam(":fecha_nacimiento", $datos["fecha_nacimiento"], PDO::PARAM_STR);

            if($stmt->execute()){

                return "ok";

            }else{

                return "error";
            
            }

            $stmt->close();
            $stmt = null;

        }

        /**
		 * Función de clase que elimina clientes en la base de datos
		 * 
		 * @param $tabla, tabla de la base de datos
		 * @param $datos, campo que indicara el registro a borrar de la base de datos
		 */
        static public function mdlBorrarCliente($tabla, $datos){

			// Consulta a la base de datos
			$stmt = Conexion::conectar() -> prepare("DELETE FROM $tabla WHERE id = :id");

			// Se asignan valores a los parametros de consulta
			$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

			// Se ejecuta la consulta
			if($stmt -> execute()){
				return "ok";
			}else{
				return "error";
			}

			// Se cierra la conexión con la base de datos
			$stmt -> close();
			$stmt = null;

		}

    }

?>