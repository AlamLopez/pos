<?php

    require_once "conexion.php";

     /**
	 * Clase que modela Productos
	 * 
	 * @package modelos
	 * @author Alam Lopez <alam.lqnpa6@hotmail.com>
	 */
    class ModeloProductos{

        /**
		 * Función de clase que muestra los productos de la base de datos
		 * 
		 * @param $tabla, tabla de la base de datos
		 * @param $item, columna de la base de datos
		 * @param $valor, valor almacenado en la base de datos
		 * @return $stmt, resultado de la consulta a la base de datos
		 */
		static public function mdlMostrarProductos($tabla, $item, $valor)
		{

			if($item != null){

				// Consulta a la base de datos
				$stmt = Conexion::conectar() -> prepare("SELECT * FROM $tabla WHERE $item = :$item ORDER BY id DESC");

				// Se asigna valor a los parametros de consulta
				$stmt -> bindParam(":" . $item, $valor, PDO::PARAM_STR);

				// Se ejecuta la consulta
				$stmt -> execute();

				// Retorna el resultado de la consulta en un array (1 fila)
				return $stmt -> fetch();
			}else{
				// Consulta a la base de datos
				$stmt = Conexion::conectar() -> prepare("SELECT * FROM $tabla");

				// Se ejecuta la consulta
				$stmt -> execute();

				// Retorna el resultado de la consulta en un array (1 fila)
				return $stmt -> fetchAll();	
			}

			// Se cierra la conexión con la base de datos
			$stmt -> close();
			$stmt = null;
			
		}

		/*=============================================
					REGISTRO DE PRODUCTO
		=============================================*/
		static public function mdlIngresarProducto($tabla, $datos){

			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(id_categoria, codigo, descripcion, imagen, stock, precio_compra, precio_venta) VALUES (:id_categoria, :codigo, :descripcion, :imagen, :stock, :precio_compra, :precio_venta)");

			$stmt->bindParam(":id_categoria", $datos["id_categoria"], PDO::PARAM_INT);
			$stmt->bindParam(":codigo", $datos["codigo"], PDO::PARAM_STR);
			$stmt->bindParam(":descripcion", $datos["descripcion"], PDO::PARAM_STR);
			$stmt->bindParam(":imagen", $datos["imagen"], PDO::PARAM_STR);
			$stmt->bindParam(":stock", $datos["stock"], PDO::PARAM_STR);
			$stmt->bindParam(":precio_compra", $datos["precio_compra"], PDO::PARAM_STR);
			$stmt->bindParam(":precio_venta", $datos["precio_venta"], PDO::PARAM_STR);

			if($stmt->execute()){

				return "ok";

			}else{

				return "error";
			
			}

			$stmt->close();
			$stmt = null;

		}

		/*=============================================
					EDICIÓN DE PRODUCTO
		=============================================*/
		static public function mdlEditarProducto($tabla, $datos){

			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET id_categoria = :id_categoria, codigo = :codigo, descripcion = :descripcion, imagen = :imagen, stock = :stock, precio_compra = :precio_compra, precio_venta = :precio_venta WHERE codigo = :codigo");

			$stmt->bindParam(":id_categoria", $datos["id_categoria"], PDO::PARAM_INT);
			$stmt->bindParam(":codigo", $datos["codigo"], PDO::PARAM_STR);
			$stmt->bindParam(":descripcion", $datos["descripcion"], PDO::PARAM_STR);
			$stmt->bindParam(":imagen", $datos["imagen"], PDO::PARAM_STR);
			$stmt->bindParam(":stock", $datos["stock"], PDO::PARAM_STR);
			$stmt->bindParam(":precio_compra", $datos["precio_compra"], PDO::PARAM_STR);
			$stmt->bindParam(":precio_venta", $datos["precio_venta"], PDO::PARAM_STR);

			if($stmt->execute()){

				return "ok";

			}else{

				return "error";
			
			}

			$stmt->close();
			$stmt = null;

		}

		/*=============================================
					ELIMINACIÓN DE PRODUCTO
		=============================================*/

		static public function mdlEliminarProducto($tabla, $datos){

			// Consulta a la base de datos
			$stmt = Conexion::conectar() -> prepare("DELETE FROM $tabla WHERE id = :id");

			// Se asignan valores a los parametros de consulta
			$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

			// Se ejecuta la consulta
			if($stmt -> execute()){
				return "ok";
			}else{
				return "error";
			}

			// Se cierra la conexión con la base de datos
			$stmt -> close();
			$stmt = null;

		}
    }
?>