/*=============================================
=              EDITAR CATEGORÍA               =
=============================================*/
$(document).on("click", ".btnEditarCliente", function() {

    // Variable que almacena el valor del atributo idCliente
    var idCliente = $(this).attr("idCliente");

    // Instancia de la clase FormData
    var datos = new FormData();

    // Se adiciona la propiedad idCliente
    datos.append("idCliente", idCliente);

    // Se aplica ajax para hacer la consulta a la BD
    $.ajax({
        url: "ajax/clientes.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {

            $("#idCliente").val(respuesta["id"]);
            $("#editarCliente").val(respuesta["nombre"]);
            $("#editarDocumentoId").val(respuesta["documento"]);
            $("#editarEmail").val(respuesta["email"]);
            $("#editarTelefono").val(respuesta["telefono"]);
            $("#editarDireccion").val(respuesta["direccion"]);
            $("#editarFechaNacimiento").val(respuesta["fecha_nacimiento"]);

        }
    });

});

/*=============================================
=              ELIMINAR CLIENTE               =
=============================================*/

$(document).on("click", ".btnEliminarCliente", function() {

    var idCliente = $(this).attr("idCliente");

    swal({
        title: '¿Está seguro de borrar el cliente?',
        text: '¡Si no está seguro puede cancelar la acción!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: '¡Sí, borrar cliente',
    }).then(function(result) {
        if (result.value) {
            window.location = 'index.php?ruta=clientes&idCliente=' + idCliente;
        }
    });
});