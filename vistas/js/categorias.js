/*=============================================
=    REVISAR SI LA CATEGORÍA ESTA REGISTRADO    =
=============================================*/

$("#nuevaCategoria").change(function() {

    $(".alert").remove();

    var categoria = $(this).val();

    var datos = new FormData();
    datos.append("validarCategoria", categoria);

    // Se aplica ajax para hacer la consulta a la BD
    $.ajax({
        url: "ajax/categorias.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {
            if (respuesta) {
                $("#nuevaCategoria").parent().after('<div class="alert alert-warning" style="text-align: center;">Esta categoría ya existe en la base de datos</div>');
                $("#nuevaCategoria").val("");
            }
        }
    });
});

/*=============================================
=              EDITAR CATEGORÍA               =
=============================================*/
$(document).on("click", ".btnEditarCategoria", function() {

    // Variable que almacena el valor del atributo idCategoria
    var idCategoria = $(this).attr("idCategoria");

    // Instancia de la clase FormData
    var datos = new FormData();

    // Se adiciona la propiedad idCategoria
    datos.append("idCategoria", idCategoria);

    // Se aplica ajax para hacer la consulta a la BD
    $.ajax({
        url: "ajax/categorias.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {

            $("#editarCategoria").val(respuesta["categoria"]);
            $("#idCategoria").val(respuesta["id"]);

        }
    });

});

/*=============================================
=              ELIMINAR CATEGORÍA               =
=============================================*/

$(document).on("click", ".btnEliminarCategoria", function() {

    var idCategoria = $(this).attr("idCategoria");

    swal({
        title: '¿Está seguro de borrar la categoría?',
        text: '¡Si no está seguro puede cancelar la acción!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: '¡Sí, borrar categoría',
    }).then(function(result) {
        if (result.value) {
            window.location = 'index.php?ruta=categorias&idCategoria=' + idCategoria;
        }
    });
});