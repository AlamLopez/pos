/*=============================================
=       SUBIENDO LA FOTO DEL USUARIO          =
=============================================*/

$(".nuevaFoto").change(function() {

    var imagen = this.files[0];

    // Validando el formato y el tamaño de la imagen
    if (imagen["type"] != "image/jpeg" && imagen["type"] != "image/png") {

        $(".nuevaFoto").val("");

        swal({
            title: "Error al subir la imagen",
            text: "!La imagen debe estar en formato PNG o JPG!",
            type: "error",
            confirmButtonText: "¡Cerrar!"
        });

    } else if (imagen["size"] > 2000000) {

        $(".nuevaFoto").val("");

        swal({
            title: "Error al subir la imagen",
            text: "!La imagen no debe pesar más de 2 MB!",
            type: "error",
            confirmButtonText: "¡Cerrar!"
        });

    } else {

        // La imagen seleccionada se previsualiza en el formulario
        var datosImagen = new FileReader;
        datosImagen.readAsDataURL(imagen);

        $(datosImagen).on("load", function(event) {
            var rutaImagen = event.target.result;
            $(".previsualizar").attr("src", rutaImagen);
        });

    }

});

/*=============================================
=              EDITAR USUARIO                 =
=============================================*/
$(document).on("click", ".btnEditarUsuario", function() {

    // Variable que almacena el valor del atributo idUsuario
    var idUsuario = $(this).attr("idUsuario");

    // Instancia de la clase FormData
    var datos = new FormData();

    // Se adiciona la propiedad idUsuario
    datos.append("idUsuario", idUsuario);

    // Se aplica ajax para hacer la consulta a la BD
    $.ajax({
        url: "ajax/usuarios.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {
            $("#editarNombre").val(respuesta["nombre"]);
            $("#editarUsuario").val(respuesta["usuario"]);
            $("#editarPerfil").html(respuesta["perfil"]);
            $("#editarPerfil").val(respuesta["perfil"]);
            $("#passwordActual").val(respuesta["password"]);
            $("#fotoActual").val(respuesta["foto"]);

            if (respuesta["foto"] != "") {
                $(".previsualizar").attr("src", respuesta["foto"]);
            } else {
                var imagenDefault = "vistas/img/usuarios/default/anonymous.png"
                $(".previsualizar").attr("src", imagenDefault);
            }
        }
    });

});

/*=============================================
=              ACTIVAR USUARIO                =
=============================================*/
$(document).on("click", ".btnActivar", function() {

    var idUsuario = $(this).attr("idUsuario");

    var estadoUsuario = $(this).attr("estadoUsuario");

    /*console.log(idUsuario);
	console.log(estadoUsuario);*/

    var datos = new FormData();
    datos.append("activarId", idUsuario);
    datos.append("activarUsuario", estadoUsuario);

    $.ajax({
        url: "ajax/usuarios.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta) {
            if (window.matchMedia("(max-width: 767px)").matches) {

                swal({
                    title: 'El usuario ha sido actualizado',
                    type: 'success',
                    confirmButtonText: '¡Cerrar!',
                }).then(function(result) {
                    if (result.value) {
                        window.location = "usuarios";
                    }
                });

            }
        }
    });

    if (estadoUsuario == 0) {
        $(this).removeClass('btn-success');
        $(this).addClass('btn-danger');
        $(this).html('Desactivado');
        $(this).attr('estadoUsuario', 1);
    } else {
        $(this).removeClass('btn-danger');
        $(this).addClass('btn-success');
        $(this).html('Activado');
        $(this).attr('estadoUsuario', 0);
    }

});

/*=============================================
=    REVISAR SI EL USUARIO ESTA REGISTRADO    =
=============================================*/

$("#nuevoUsuario").change(function() {

    $(".alert").remove();

    var usuario = $(this).val();

    var datos = new FormData();
    datos.append("validarUsuario", usuario);

    // Se aplica ajax para hacer la consulta a la BD
    $.ajax({
        url: "ajax/usuarios.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {
            if (respuesta) {
                $("#nuevoUsuario").parent().after('<div class="alert alert-warning" style="text-align: center;">Este usuario ya existe en la base de datos</div>');
                $("#nuevoUsuario").val("");
            }
        }
    });
});

/*=============================================
=              ELIMINAR USUARIO               =
=============================================*/

$(document).on("click", ".btnEliminarUsuario", function() {

    var idUsuario = $(this).attr("idUsuario");
    var fotoUsuario = $(this).attr("fotoUsuario");
    var usuario = $(this).attr("usuario");

    swal({
        title: '¿Está seguro de borrar el usuario?',
        text: '¡Si no está seguro puede cancelar la acción!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: '¡Sí, borrar usuario!',
    }).then(function(result) {
        if (result.value) {
            window.location = 'index.php?ruta=usuarios&idUsuario=' + idUsuario + '&usuario=' + usuario + '&fotoUsuario=' + fotoUsuario;
        }
    });
});