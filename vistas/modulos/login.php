<div id="back"></div>

<div class="login-box">
  
  <!-- Logotipo de la aplicación -->
  <div class="login-logo">
    
    <img src="vistas/img/plantilla/logo-blanco-bloque.png" class="img-responsive" style="padding: 30px 100px 0px 100px">

  </div>
  
  <!-- Contenido del login -->
  <div class="login-box-body">
    
    <p class="login-box-msg">Ingresar al sistema</p>

    <!-- Inicia el formulario -->
    <form method="post">
      
      <!-- Usuario -->
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Usuario" name="ingUsuario" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      
      <!-- Contraseña -->
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Contraseña" name="ingPassword" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>

      <!-- Botón de ingreso -->
      <div class="row">
        
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>
        </div>
        
      </div>

      <?php

        $login = new ControladorUsuarios();
        $login -> ctrIngresoUsuario();

      ?>

    </form>

  </div>
  
</div>
