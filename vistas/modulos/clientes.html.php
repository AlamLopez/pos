<div class="content-wrapper">
  <!-- Encabezado de contenido (encabezado de página) -->
  <section class="content-header">
    
    <h1>
      Administrar clientes
    </h1>
    
    <ol class="breadcrumb">
      <li>
        <a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a>
      </li>
      <li class="active">Administrar clientes</li>
    </ol>

  </section>

  <!-- Contenido principal -->
  <section class="content">

    <div class="box">

      <div class="box-header with-border">
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarCliente">
          Agregar Cliente
        </button> 
      </div>
      
      <div class="box-body">
        
        <table class="table table-bordered table-striped dt-responsive tablas">

          <thead>
            <tr>
              <th style="width: 10px">#</th>
              <th>NOMBRE</th>
              <th>DOCUMENTO ID</th>
              <th>EMAIL</th>
              <th>TELÉFONO</th>
              <th>DIRECCIÓN</th>
              <th>FECHA DE NACIMIENTO</th>
              <th>TOTAL COMPRAS</th>
              <th>ÚLTIMA COMPRA</th>
              <th>INGRESO AL SISTEMA</th>
              <th>ACCIONES</th>
            </tr>
          </thead>

          <tbody>
            
            <tr>
              <td>1</td>
              <td>Juan Villegas</td>
              <td>05187238-7</td>
              <td>juan@hotmail.com</td>
              <td>2292-1869</td>
              <td>Calle Alternador #345</td>
              <td>1995-05-06</td>
              <td>35</td>
              <td>2018-05-07 12:05:32</td>
              <td>2000-07-11 20:03:21</td>
              <td>
                <div class="btn-group">
                  <button class="btn btn-warning"><i class="fa fa-pencil"></i></button>
                  <button class="btn btn-danger"><i class="fa fa-times"></i></button>
                </div>
              </td>
            </tr>

            <tr>
              <td>1</td>
              <td>Juan Villegas</td>
              <td>05187238-7</td>
              <td>juan@hotmail.com</td>
              <td>2292-1869</td>
              <td>Calle Alternador #345</td>
              <td>1995-05-06</td>
              <td>35</td>
              <td>2018-05-07 12:05:32</td>
              <td>2000-07-11 20:03:21</td>
              <td>
                <div class="btn-group">
                  <button class="btn btn-warning"><i class="fa fa-pencil"></i></button>
                  <button class="btn btn-danger"><i class="fa fa-times"></i></button>
                </div>
              </td>
            </tr>

            <tr>
              <td>1</td>
              <td>Juan Villegas</td>
              <td>05187238-7</td>
              <td>juan@hotmail.com</td>
              <td>2292-1869</td>
              <td>Calle Alternador #345</td>
              <td>1995-05-06</td>
              <td>35</td>
              <td>2018-05-07 12:05:32</td>
              <td>2000-07-11 20:03:21</td>
              <td>
                <div class="btn-group">
                  <button class="btn btn-warning"><i class="fa fa-pencil"></i></button>
                  <button class="btn btn-danger"><i class="fa fa-times"></i></button>
                </div>
              </td>
            </tr>

          </tbody>
          
        </table>

      </div>
      
    </div>
    
  </section>
  
</div>

<!--=====================================
=        MODAL AGREGAR CLIENTE          =
======================================-->

<div id="modalAgregarCliente" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">
      
      <form action="post" role="form">
        
        <!-- Encabezado del modal -->
        <div class="modal-header" style="background: #3c8dbc; color: white">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><b>Agregar Cliente</b></h4>
        </div>
        
        <!-- Contenido del modal -->
        <div class="modal-body">
          
          <div class="box-body">
            
            <!-- Entrada para el nombre -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-user" style="color: white;"></i></span>
                <input type="text" class="form-control input-group-lg" name="nuevoCliente" placeholder="Ingresar cliente" required>
              </div>
            </div>

            <!-- Entrada para el documento ID -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-key" style="color: white;"></i></span>
                <input type="number" min="0" class="form-control input-group-lg" name="nuevoDocumentoId" placeholder="Ingresar documento ID" required>
              </div>
            </div>

            <!-- Entrada para el email -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-envelope" style="color: white;"></i></span>
                <input type="email" class="form-control input-group-lg" name="nuevoEmail" placeholder="Ingresar email" required>
              </div>
            </div>

            <!-- Entrada para el teléfono -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-phone" style="color: white;"></i></span>
                <input type="text" class="form-control input-group-lg" name="nuevoTelefono" placeholder="Ingresar teléfono" data-inputmask="'mask':'(999) 9999-9999'" data-mask required>
              </div>
            </div>

            <!-- Entrada para la dirección -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-map-marker" style="color: white;"></i></span>
                <input type="text" class="form-control input-group-lg" name="nuevaDireccion" placeholder="Ingresar dirección" required>
              </div>
            </div>
            
            <!-- Entrada para el teléfono -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-calendar" style="color: white;"></i></span>
                <input type="text" class="form-control input-group-lg" name="nuevaFechaNacimiento" placeholder="Ingresar fecha de nacimiento" data-inputmask="'alias':'yyyy/mm/dd'" data-mask required>
              </div>
            </div>

          </div>

        </div>
        
        <!-- Footer del modal -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Guardar</button>

        </div>

      </form>

    </div>

  </div>

</div>