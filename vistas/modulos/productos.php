<div class="content-wrapper">
  <!-- Encabezado de contenido (encabezado de página) -->
  <section class="content-header">
    
    <h1>
      Administrar productos
    </h1>
    
    <ol class="breadcrumb">
      <li>
        <a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a>
      </li>
      <li class="active">Administrar productos</li>
    </ol>

  </section>

  <!-- Contenido principal -->
  <section class="content">

    <div class="box">

      <div class="box-header with-border">
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarProducto">
          Agregar Producto
        </button> 
      </div>
      
      <div class="box-body">
        
        <table class="table table-bordered table-striped dt-responsive tablaProductos text-uppercase" style="text-align: center;" width="100%">

          <thead>
            <tr>
              <th style="width: 10px; text-align: center;">#</th>
              <th style="text-align: center;">IMAGEN</th>
              <th style="text-align: center;">CÓDIGO</th>
              <th style="text-align: center;">DESCRIPCIÓN</th>
              <th style="text-align: center;">CATEGORÍA</th>
              <th style="text-align: center;">STOCK</th>
              <th style="text-align: center;">PRECIO DE COMPRA</th>
              <th style="text-align: center;">PRECIO DE VENTA</th>
              <th style="text-align: center;">AGREGADO</th>
              <th style="text-align: center;">ACCIONES</th>
            </tr>
          </thead>

        </table>

      </div>
      
    </div>
    
  </section>
  
</div>

<!--=====================================
=        MODAL AGREGAR PRODUCTO         =
======================================-->

<div id="modalAgregarProducto" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">
      
      <form method="post" role="form" enctype="multipart/form-data" autocomplete="off">
        
        <!-- Encabezado del modal -->
        <div class="modal-header" style="background: #3c8dbc; color: white">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><b>Agregar Producto</b></h4>
        </div>
        
        <!-- Contenido del modal -->
        <div class="modal-body">
          
          <div class="box-body">

            <!-- Entrada para la categoría -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-th" style="color: white;"></i></span>
                <select class="form-control input-group-lg" name="nuevaCategoria" id="nuevaCategoria" required>
                  <option value="">SELECCIONAR CATEGORÍA</option>
                  <?php

                    $item = null;
                    $valor = null;

                    $categorias = ControladorCategorias::ctrMostrarCategorias($item, $valor);

                    foreach ($categorias as $key => $value) {
                      echo '<option value="'.$value["id"].'" class= "text-uppercase">'.$value["categoria"].'</option>';
                    }
                  ?>
                </select>
              </div>
            </div>
            
            <!-- Entrada para el código -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-code" style="color: white;"></i></span>
                <input type="text" class="form-control input-group-lg" id="nuevoCodigo" name="nuevoCodigo" placeholder="Ingresar código" required readonly>
              </div>
            </div>

            <!-- Entrada para la descripción -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-product-hunt" style="color: white;"></i></span>
                <input type="text" class="form-control input-group-lg" name="nuevaDescripcion" placeholder="Ingresar descripción" required>
              </div>
            </div>

            <!-- Entrada para el stock -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-check" style="color: white;"></i></span>
                <input type="number" class="form-control input-group-lg" name="nuevoStock" min="0" placeholder="Stock" required>
              </div>
            </div>

            <!-- Entrada para el precio compra -->
            <div class="form-group row">
              <div class="col-xs-12 col-sm-6">
                <div class="input-group">
                  <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-arrow-up" style="color: white;"></i></span>
                  <input type="number" class="form-control input-group-lg" id="nuevoPrecioCompra" name="nuevoPrecioCompra" min="0" step="any" placeholder="Precio de compra" required>
                </div>
              </div>

            <!-- Entrada para el precio de venta -->
              <div class="col-xs-12 col-sm-6">
                <div class="input-group">
                  <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-arrow-down" style="color: white;"></i></span>
                  <input type="number" class="form-control input-group-lg" id="nuevoPrecioVenta" name="nuevoPrecioVenta" min="0" step="any" placeholder="Precio de venta" required>
                </div>
                <br>
                <!-- Checkbox para porcentaje -->
                <div class="col-xs-6">
                  <div class="form-group">
                    <label>
                      <input type="checkbox" class="minimal porcentaje" checked>
                      Utilizar porcentaje
                    </label>
                  </div>
                </div>
                <!-- Entrada para porcentaje -->
                <div class="col-xs-6" style="padding:0;">
                  <div class="input-group">
                    <input type="number" class="form-control input-lg nuevoPorcentaje" min="0" value="40" required>
                    <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-percent" style="color: white;"></i></span>
                  </div>
                </div>
              </div>
            </div>

            <!-- Entrada para subir imagen -->
            <div class="form-group">
              <div class="panel">SUBIR IMAGEN</div>
              <input type="file" class="nuevaImagen" name="nuevaImagen">
              <p class="help-block">Peso máximo de la foto 2MB</p>
              <img src="vistas/img/productos/default/anonymous.png" class="img-thumbnail previsualizar" width="100px">
            </div>

          </div>

        </div>
        
        <!-- Footer del modal -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Guardar</button>

        </div>

      </form>

      <?php
        $crearProducto = new ControladorProductos();
        $crearProducto -> ctrCrearProducto();
      ?>

    </div>

  </div>

</div>

<!--=====================================
=         MODAL EDITAR PRODUCTO         =
======================================-->

<div id="modalEditarProducto" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">
      
      <form method="post" role="form" enctype="multipart/form-data" autocomplete="off">
        
        <!-- Encabezado del modal -->
        <div class="modal-header" style="background: #3c8dbc; color: white">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><b>Editar Producto</b></h4>
        </div>
        
        <!-- Contenido del modal -->
        <div class="modal-body">
          
          <div class="box-body">

            <!-- Entrada para la categoría -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-th" style="color: white;"></i></span>
                <select class="form-control input-group-lg" name="editarCategoria" readonly required>
                  <option id="editarCategoria"></option>
                </select>
              </div>
            </div>
            
            <!-- Entrada para el código -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-code" style="color: white;"></i></span>
                <input type="text" class="form-control input-group-lg" id="editarCodigo" name="editarCodigo" required readonly>
              </div>
            </div>

            <!-- Entrada para la descripción -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-product-hunt" style="color: white;"></i></span>
                <input type="text" class="form-control input-group-lg" id="editarDescripcion" name="editarDescripcion" required>
              </div>
            </div>

            <!-- Entrada para el stock -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-check" style="color: white;"></i></span>
                <input type="number" class="form-control input-group-lg" id="editarStock" name="editarStock" min="0" required>
              </div>
            </div>

            <!-- Entrada para el precio compra -->
            <div class="form-group row">
              <div class="col-xs-12 col-sm-6">
                <div class="input-group">
                  <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-arrow-up" style="color: white;"></i></span>
                  <input type="number" class="form-control input-group-lg" id="editarPrecioCompra" name="editarPrecioCompra" min="0" step="any" required>
                </div>
              </div>

            <!-- Entrada para el precio de venta -->
              <div class="col-xs-12 col-sm-6">
                <div class="input-group">
                  <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-arrow-down" style="color: white;"></i></span>
                  <input type="number" class="form-control input-group-lg" id="editarPrecioVenta" name="editarPrecioVenta" min="0" step="any" placeholder="Precio de venta" required readonly>
                </div>
                <br>
                <!-- Checkbox para porcentaje -->
                <div class="col-xs-6">
                  <div class="form-group">
                    <label>
                      <input type="checkbox" class="minimal porcentaje" checked>
                      Utilizar porcentaje
                    </label>
                  </div>
                </div>
                <!-- Entrada para porcentaje -->
                <div class="col-xs-6" style="padding:0;">
                  <div class="input-group">
                    <input type="number" class="form-control input-lg nuevoPorcentaje" min="0" value="40" required>
                    <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-percent" style="color: white;"></i></span>
                  </div>
                </div>
              </div>
            </div>

            <!-- Entrada para subir imagen -->
            <div class="form-group">
              <div class="panel">SUBIR IMAGEN</div>
              <input type="file" class="nuevaImagen" name="editarImagen">
              <p class="help-block">Peso máximo de la foto 2MB</p>
              <img src="vistas/img/productos/default/anonymous.png" class="img-thumbnail previsualizar" width="100px">
              <input type="hidden" name="imagenActual" id="imagenActual">
            </div>

          </div>

        </div>
        
        <!-- Footer del modal -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Guardar cambios</button>

        </div>

      </form>

      <?php
      
        $editarProducto = new ControladorProductos();
        $editarProducto -> ctrEditarProducto();
        
      ?>

    </div>

  </div>

</div>

<?php
      
      $eliminarProducto = new ControladorProductos();
      $eliminarProducto -> ctrEliminarProducto();
      
    ?>