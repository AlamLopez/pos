<div class="content-wrapper">
  <!-- Encabezado de contenido (encabezado de página) -->
  <section class="content-header">
    
    <h1>
      Página no encontrada
    </h1>
    
    <ol class="breadcrumb">
      <li>
        <a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a>
      </li>
      <li class="active">Página no encontrada</li>
    </ol>

  </section>

  <!-- Main content -->
  <section class="content">

    <div class="error-page">
      
      <h2 class="headline text-primary">404</h2>
      
      <div class="error-content">
        
        <h3>
          
          <i class="fa fa-warning text-primary"></i>
          
          <b>Oooppss!!!! Página no encontrada</b>

          <p style="text-align: justify;">
            Utiliza al menú lateral, y ahi podrás encontrar las 
            páginas disponibles. Tambien puedes regresar haciendo 
            click <a href="inicio">aquí</a>
          </p>


        </h3>

      </div>

    </div>
    
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->