<div class="content-wrapper">
  <!-- Encabezado de contenido (encabezado de página) -->
  <section class="content-header">
    
    <h1>
      Administrar clientes
    </h1>
    
    <ol class="breadcrumb">
      <li>
        <a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a>
      </li>
      <li class="active">Administrar clientes</li>
    </ol>

  </section>

  <!-- Contenido principal -->
  <section class="content">

    <div class="box">

      <div class="box-header with-border">
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarCliente">
          Agregar Cliente
        </button> 
      </div>
      
      <div class="box-body">
        
        <table class="table table-bordered table-striped dt-responsive tablas" style="text-align: center;" width="100%">

          <thead>
            <tr>
              <th style="width: 10px" style="text-align: center;">#</th>
              <th style="text-align: center;">NOMBRE</th>
              <th style="text-align: center;">DOCUMENTO ID</th>
              <th style="text-align: center;">EMAIL</th>
              <th style="text-align: center;">TELÉFONO</th>
              <th style="text-align: center;">DIRECCIÓN</th>
              <th style="text-align: center;">FECHA DE NACIMIENTO</th>
              <th style="text-align: center;">TOTAL COMPRAS</th>
              <th style="text-align: center;">ÚLTIMA COMPRA</th>
              <th style="text-align: center;">INGRESO AL SISTEMA</th>
              <th style="text-align: center;">ACCIONES</th>
            </tr>
          </thead>

          <tbody>
            
            <?php

              $item = null;
              $datos = null;

              $clientes = ControladorClientes::ctrMostrarClientes($item, $datos);

             foreach ($clientes as $key => $value) {
               
                echo '<tr>
                        <td class = "text-uppercase">'.($key+1).'</td>
                        <td class = "text-uppercase">'.$value["nombre"].'</td>
                        <td>'.$value["documento"].'</td>
                        <td>'.$value["email"].'</td>
                        <td>'.$value["telefono"].'</td>
                        <td class = "text-uppercase">'.$value["direccion"].'</td>
                        <td>'.$value["fecha_nacimiento"].'</td>             
                        <td>'.$value["compras"].'</td>
                        <td>0000-00-00 00:00:00</td>
                        <td>'.$value["fecha"].'</td>
                        <td>
                          <div class="btn-group">
                            <button class="btn btn-warning btnEditarCliente" data-toggle="modal" data-target="#modalEditarCliente" idCliente="'.$value["id"].'"><i class="fa fa-pencil"></i></button>
                            <button class="btn btn-danger btnEliminarCliente" idCliente="'.$value["id"].'"><i class="fa fa-times"></i></button>
                          </div>  
                        </td>
                      </tr>';

             }

            ?>
            
          </tbody>
          
        </table>

      </div>
      
    </div>
    
  </section>
  
</div>

<!--=====================================
=        MODAL AGREGAR CLIENTE          =
======================================-->

<div id="modalAgregarCliente" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">
      
      <form method="post" role="form" autocomplete="off">
        
        <!-- Encabezado del modal -->
        <div class="modal-header" style="background: #3c8dbc; color: white">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><b>Agregar Cliente</b></h4>
        </div>
        
        <!-- Contenido del modal -->
        <div class="modal-body">
          
          <div class="box-body">
            
            <!-- Entrada para el nombre -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-user" style="color: white;"></i></span>
                <input type="text" class="form-control input-group-lg" name="nuevoCliente" placeholder="Ingresar cliente" required>
              </div>
            </div>

            <!-- Entrada para el documento ID -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-key" style="color: white;"></i></span>
                <input type="number" min="0" class="form-control input-group-lg" name="nuevoDocumentoId" placeholder="Ingresar documento ID" required>
              </div>
            </div>

            <!-- Entrada para el email -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-envelope" style="color: white;"></i></span>
                <input type="email" class="form-control input-group-lg" name="nuevoEmail" placeholder="Ingresar email" required>
              </div>
            </div>

            <!-- Entrada para el teléfono -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-phone" style="color: white;"></i></span>
                <input type="text" class="form-control input-group-lg" name="nuevoTelefono" placeholder="Ingresar teléfono" data-inputmask="'mask':'(999) 9999-9999'" data-mask required>
              </div>
            </div>

            <!-- Entrada para la dirección -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-map-marker" style="color: white;"></i></span>
                <input type="text" class="form-control input-group-lg" name="nuevaDireccion" placeholder="Ingresar dirección" required>
              </div>
            </div>
            
            <!-- Entrada para la fecha de nacimiento -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-calendar" style="color: white;"></i></span>
                <input type="text" class="form-control input-group-lg" name="nuevaFechaNacimiento" placeholder="Ingresar fecha de nacimiento" data-inputmask="'alias':'yyyy/mm/dd'" data-mask required>
              </div>
            </div>

          </div>

        </div>
        
        <!-- Footer del modal -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Guardar</button>

        </div>

      </form>

      <?php

        $crearCliente = new ControladorClientes();
        $crearCliente -> ctrCrearCliente();

      ?>

    </div>

  </div>

</div>

<!--=====================================
=        MODAL EDITAR CLIENTE          =
======================================-->

<div id="modalEditarCliente" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">
      
      <form method="post" role="form" autocomplete="off">
        
        <!-- Encabezado del modal -->
        <div class="modal-header" style="background: #3c8dbc; color: white">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><b>Editar Cliente</b></h4>
        </div>
        
        <!-- Contenido del modal -->
        <div class="modal-body">
          
          <div class="box-body">
            
            <!-- Entrada para el nombre -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-user" style="color: white;"></i></span>
                <input type="text" class="form-control input-group-lg" id="editarCliente" name="editarCliente" required>
                <input type="hidden" id="idCliente" name="idCliente">
              </div>
            </div>

            <!-- Entrada para el documento ID -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-key" style="color: white;"></i></span>
                <input type="number" min="0" class="form-control input-group-lg" name="editarDocumentoId" id="editarDocumentoId" required>
              </div>
            </div>

            <!-- Entrada para el email -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-envelope" style="color: white;"></i></span>
                <input type="email" class="form-control input-group-lg" name="editarEmail" id="editarEmail" required>
              </div>
            </div>

            <!-- Entrada para el teléfono -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-phone" style="color: white;"></i></span>
                <input type="text" class="form-control input-group-lg" name="editarTelefono" id="editarTelefono" data-inputmask="'mask':'(999) 9999-9999'" data-mask required>
              </div>
            </div>

            <!-- Entrada para la dirección -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-map-marker" style="color: white;"></i></span>
                <input type="text" class="form-control input-group-lg" name="editarDireccion" id="editarDireccion" required>
              </div>
            </div>
            
            <!-- Entrada para la fecha de nacimiento -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" style="background: #3c8dbc;"><i class="fa fa-calendar" style="color: white;"></i></span>
                <input type="text" class="form-control input-group-lg" name="editarFechaNacimiento" id="editarFechaNacimiento" data-inputmask="'alias':'yyyy/mm/dd'" data-mask required>
              </div>
            </div>

          </div>

        </div>
        
        <!-- Footer del modal -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Guardar Cambios</button>

        </div>

      </form>

      <?php

        $editarCliente = new ControladorClientes();
        $editarCliente -> ctrEditarCliente();

      ?>

    </div>

  </div>

</div>

<?php

        $eliminarCliente = new ControladorClientes();
        $eliminarCliente -> ctrBorrarCliente();

?>