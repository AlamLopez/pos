<div class="content-wrapper">
  <!-- Encabezado de contenido (encabezado de página) -->
  <section class="content-header">
    
    <h1>
      Administrar categorías
    </h1>
    
    <ol class="breadcrumb">
      <li>
        <a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a>
      </li>
      <li class="active">Administrar categorías</li>
    </ol>

  </section>

  <!-- Contenido principal -->
  <section class="content">

    <div class="box">

      <div class="box-header with-border">
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarCategoria">
          Agregar Categoría
        </button> 
      </div>
      
      <div class="box-body">
        
        <table class="table table-bordered table-striped dt-responsive tablas" style="text-align: center;" width="100%">

          <thead>
            <tr>
              <th style="width: 10px" style="text-align: center;">#</th>
              <th style="text-align: center;">CATEGORÍA</th>
              <th style="text-align: center;">ACCIONES</th>
            </tr>
          </thead>

          <tbody>

            <?php
              $item = null;
              $valor = null;

              $categorias = ControladorCategorias::ctrMostrarCategorias($item, $valor);

              foreach ($categorias as $key => $value) {
                echo  '<tr>
                        <td>' . ($key + 1) .'</td>
                        <td class = "text-uppercase">' . $value["categoria"] . '</td>
                        <td>
                          <div class="btn-group">
                            <button class="btn btn-warning btnEditarCategoria" idCategoria="'.$value["id"].'" data-toggle="modal" data-target="#modalEditarCategoria"><i class="fa fa-pencil"></i></button>
                            <button class="btn btn-danger btnEliminarCategoria" idCategoria="'.$value["id"].'"><i class="fa fa-times"></i></button>
                          </div>
                        </td>
                      </tr>';
              }
            ?>

          </tbody>
          
        </table>

      </div>
      
    </div>
    
  </section>
  
</div>

<!--=====================================
=        MODAL AGREGAR CATEGORÍA          =
======================================-->

<div id="modalAgregarCategoria" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">
      
      <form method="post" role="form" autocomplete="off">
        
        <!-- Encabezado del modal -->
        <div class="modal-header" style="background: #3c8dbc; color: white">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Agregar Categoría</h4>
        </div>
        
        <!-- Contenido del modal -->
        <div class="modal-body">
          
          <div class="box-body">
            
            <!-- Entrada para el nombre -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-th"></i></span>
                <input type="text" class="form-control input-group-lg" id="nuevaCategoria" name="nuevaCategoria" placeholder="Ingresar categoría" required autofocus>
              </div>
            </div>

          </div>

        </div>
        
        <!-- Footer del modal -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Guardar</button>

        </div>

        <?php
          $crearCategoria = new ControladorCategorias();
          $crearCategoria -> ctrCrearCategoria();
        ?>

      </form>

    </div>

  </div>

</div>

<!--=====================================
=        MODAL EDITAR CATEGORÍA          =
======================================-->

<div id="modalEditarCategoria" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">
      
      <form method="post" role="form" autocomplete="off">
        
        <!-- Encabezado del modal -->
        <div class="modal-header" style="background: #3c8dbc; color: white">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar Categoría</h4>
        </div>
        
        <!-- Contenido del modal -->
        <div class="modal-body">
          
          <div class="box-body">
            
            <!-- Entrada para el nombre -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-th"></i></span>
                <input type="text" class="form-control input-group-lg" id="editarCategoria" name="editarCategoria" required>
                <input type="hidden" id="idCategoria" name="idCategoria" required>
              </div>
            </div>

          </div>

        </div>
        
        <!-- Footer del modal -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Guardar cambios</button>

        </div>

        <?php
          $editarCategoria = new ControladorCategorias();
          $editarCategoria -> ctrEditarCategoria();
        ?>

      </form>

    </div>

  </div>

</div>

<?php
    $borrarCategoria = new ControladorCategorias();
    $borrarCategoria -> ctrBorrarCategoria();
?>