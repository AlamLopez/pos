<div class="content-wrapper">
  <!-- Encabezado de contenido (encabezado de página) -->
  <section class="content-header">
    
    <h1>
      Administrar productos
    </h1>
    
    <ol class="breadcrumb">
      <li>
        <a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a>
      </li>
      <li class="active">Administrar productos</li>
    </ol>

  </section>

  <!-- Contenido principal -->
  <section class="content">

    <div class="box">

      <div class="box-header with-border">
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarProducto">
          Agregar Producto
        </button> 
      </div>
      
      <div class="box-body">
        
        <table class="table table-bordered table-striped dt-responsive tablas">

          <thead>
            <tr>
              <th style="width: 10px">#</th>
              <th>IMAGEN</th>
              <th>CÓDIGO</th>
              <th>DESCRIPCIÓN</th>
              <th>CATEGORÍA</th>
              <th>STOCK</th>
              <th>PRECIO DE COMPRA</th>
              <th>PRECIO DE VENTA</th>
              <th>AGREGADO</th>
              <th>ACCIONES</th>
            </tr>
          </thead>

          <tbody>
            
            <tr>
              <td>1</td>
              <td><img src="vistas/img/productos/default/anonymous.png" class="img-thumbnail" width="40px"></td>
              <td>0001</td>
              <td>Lorem ipsum dolor sit amet consectetur adipisicing elit</td>
              <td>Lorem ipsum dolor</td>
              <td>40</td>
              <td>$ 5.00</td>
              <td>$ 10.00</td>
              <td>2018-07-29 23:35:33</td>
              <td>
                <div class="btn-group">
                  <button class="btn btn-warning"><i class="fa fa-pencil"></i></button>
                  <button class="btn btn-danger"><i class="fa fa-times"></i></button>
                </div>
              </td>
            </tr>

            <tr>
              <td>1</td>
              <td><img src="vistas/img/productos/default/anonymous.png" class="img-thumbnail" width="40px"></td>
              <td>0001</td>
              <td>Lorem ipsum dolor sit amet consectetur adipisicing elit</td>
              <td>Lorem ipsum dolor</td>
              <td>40</td>
              <td>$ 5.00</td>
              <td>$ 10.00</td>
              <td>2018-07-29 23:35:33</td>
              <td>
                <div class="btn-group">
                  <button class="btn btn-warning"><i class="fa fa-pencil"></i></button>
                  <button class="btn btn-danger"><i class="fa fa-times"></i></button>
                </div>
              </td>
            </tr>

            <tr>
              <td>1</td>
              <td><img src="vistas/img/productos/default/anonymous.png" class="img-thumbnail" width="40px"></td>
              <td>0001</td>
              <td>Lorem ipsum dolor sit amet consectetur adipisicing elit</td>
              <td>Lorem ipsum dolor</td>
              <td>40</td>
              <td>$ 5.00</td>
              <td>$ 10.00</td>
              <td>2018-07-29 23:35:33</td>
              <td>
                <div class="btn-group">
                  <button class="btn btn-warning"><i class="fa fa-pencil"></i></button>
                  <button class="btn btn-danger"><i class="fa fa-times"></i></button>
                </div>
              </td>
            </tr>

          </tbody>
          
        </table>

      </div>
      
    </div>
    
  </section>
  
</div>

<!--=====================================
=        MODAL AGREGAR PRODUCTO         =
======================================-->

<div id="modalAgregarProducto" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">
      
      <form method="post" role="form" enctype="multipart/form-data" autocomplete="off">
        
        <!-- Encabezado del modal -->
        <div class="modal-header" style="background: #3c8dbc; color: white">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Agregar Producto</h4>
        </div>
        
        <!-- Contenido del modal -->
        <div class="modal-body">
          
          <div class="box-body">
            
            <!-- Entrada para el código -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-code"></i></span>
                <input type="text" class="form-control input-group-lg" name="nuevoCodigo" placeholder="Ingresar código" required>
              </div>
            </div>

            <!-- Entrada para la descripción -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-product-hunt"></i></span>
                <input type="text" class="form-control input-group-lg" name="nuevaDescripcion" placeholder="Ingresar descripción" required>
              </div>
            </div>

            <!-- Entrada para la categoría -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-th"></i></span>
                <select class="form-control input-group-lg" name="nuevaCategoria">
                  <option value="">Seleccionar categoría</option>
                  <option value="Taladros">Taladros</option>
                  <option value="Andamios">Andamios</option>
                  <option value="Equipos para construcción">Equipos para construcción</option>
                </select>
              </div>
            </div>

            <!-- Entrada para el stock -->
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-check"></i></span>
                <input type="number" class="form-control input-group-lg" name="nuevoStock" min="0" placeholder="Stock" required>
              </div>
            </div>

            <!-- Entrada para el precio compra -->
            <div class="form-group row">
              <div class="col-xs-6">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-arrow-up"></i></span>
                  <input type="number" class="form-control input-group-lg" name="nuevoPrecioCompra" min="0" placeholder="Precio de compra" required>
                </div>
              </div>

            <!-- Entrada para el precio de venta -->
              <div class="col-xs-6">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-arrow-down"></i></span>
                  <input type="number" class="form-control input-group-lg" name="nuevoPrecioVenta" min="0" placeholder="Precio de venta" required>
                </div>
                <br>
                <!-- Checkbox para porcentaje -->
                <div class="col-xs-6">
                  <div class="form-group">
                    <label>
                      <input type="checkbox" class="minimal porcentaje" checked>
                      Utilizar porcentaje
                    </label>
                  </div>
                </div>
                <!-- Entrada para porcentaje -->
                <div class="col-xs-6" style="padding:0;">
                  <div class="input-group">
                    <input type="number" class="form-control input-lg nuevoPorcentaje" min="0" value="40" required>
                    <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                  </div>
                </div>
              </div>
            </div>

            <!-- Entrada para subir imagen -->
            <div class="form-group">
              <div class="panel">SUBIR IMAGEN</div>
              <input type="file" id="nuevaImagen" name="nuevaImagen">
              <p class="help-block">Peso máximo de la foto 2MB</p>
              <img src="vistas/img/productos/default/anonymous.png" class="img-thumbnail" width="100px">
            </div>

          </div>

        </div>
        
        <!-- Footer del modal -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" class="btn btn-primary">Guardar</button>

        </div>

      </form>

    </div>

  </div>

</div>