<?php  
    
    /**
	 * Clase que gestiona las categorias de la aplicación
	 * 
	 * @package controladores
	 * @author Alam Lopez <alam.lqnpa6@hotmail.com>
	 */
	class ControladorCategorias{

		/*=============================================
		=             CREACIÓN DE CATEGORÍAS            =
		=============================================*/

		/**
		 * Función que recibe datos para ingresar una 
		 * nueva categoría a la aplicación
		 */
        static public function ctrCrearCategoria(){

            // Se verifica que se recibe la variable POST desde la vista categorias.php
            if (isset($_POST["nuevaCategoria"])) {

				
				/* Verifica que nuevaCategoria solo acepte caracteres alfanuméricos */
                if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevaCategoria"])){

					// Tabla de la BD en la que se insertaran datos
                    $tabla = "categorias";

					// Valor que será insertado
                    $datos = $_POST["nuevaCategoria"];

					// Solicita respuesta del modelo
					$respuesta = ModeloCategorias::mdlIngresarCategoria($tabla, $datos);

					/* Si la respuesta es ok se muestra el SweetAlert de éxito, sino se muestra el SweetAlert de error*/
                    if($respuesta == "ok"){
                     
                        echo '<script>

									swal({

										type: "success",
										title: "¡La categoría ha sido guardada exitosamente!",
										showConfirmButton: true,
                                        confirmButtonText: "Cerrar",

									}).then(function(result){

										if(result.value){
						
											window.location = "categorias";

										}

									});
				
                             </script>';
                             
                    }

                }else{
                    echo '<script>

									swal({

										type: "error",
										title: "¡La categoría no puede ir vacía o llevar caracteres especiales!",
										showConfirmButton: true,
                                        confirmButtonText: "Cerrar",
                                        closeOnConfirm: false

									}).then(function(result){

										if(result.value){
						
											window.location = "categorias";

										}

									});
				
		    		 		</script>';
                }
            }
		}
		
		/*=============================================
		=            CONSULTA DE PRODUCTOS            =
		=============================================*/

		/**
		 * Función que recibe datos para ingresar una 
		 * nueva categoría a la aplicación
		 */
		static public function ctrMostrarCategorias($item, $valor){

			$tabla = "categorias";
			$respuesta = ModeloCategorias::MdlMostrarCategorias($tabla, $item, $valor);
			return $respuesta;
		}

		/*=============================================
		=             EDICIÓN DE CATEGORÍAS            =
		=============================================*/

		/**
		 * Función que recibe datos para editar
		 * una categoría a la aplicación
		 */
        static public function ctrEditarCategoria(){

            // Se verifica que se recibe la variable POST desde la vista categorias.php
            if (isset($_POST["editarCategoria"])) {

				
				/* Verifica que nuevaCategoria solo acepte caracteres alfanuméricos */
                if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["editarCategoria"])){

					// Tabla de la BD en la que se editaran datos
					$tabla = "categorias";

					// Valor que será editado
					$datos = array("categoria" => $_POST["editarCategoria"],
									"id" => $_POST["idCategoria"]);

					// Solicita respuesta del modelo
					$respuesta = ModeloCategorias::mdlEditarCategoria($tabla, $datos);

					/* Si la respuesta es ok se muestra el SweetAlert de éxito, sino se muestra el SweetAlert de error*/
                    if($respuesta == "ok"){
                     
                        echo '<script>

									swal({

										type: "success",
										title: "¡La categoría ha sido editada exitosamente!",
										showConfirmButton: true,
                                        confirmButtonText: "Cerrar",

									}).then(function(result){

										if(result.value){
						
											window.location = "categorias";

										}

									});
				
                             </script>';
                             
                    }

                }else{
                    echo '<script>

									swal({

										type: "error",
										title: "¡La categoría no puede ir vacía o llevar caracteres especiales!",
										showConfirmButton: true,
                                        confirmButtonText: "Cerrar",
                                        closeOnConfirm: false

									}).then(function(result){

										if(result.value){
						
											window.location = "categorias";

										}

									});
				
		    		 		</script>';
                }
            }
		}

		/*=============================================
		=          ELIMINACIÓN DE CATEGORÍAS          =
		=============================================*/

		/**
		 * Función que recibe datos para eliminar  
		 * una categoría de la aplicación segun su ID
		 */

		static public function ctrBorrarCategoria(){

			if(isset($_GET["idCategoria"])){

				$tabla = "categorias";
				$datos = $_GET["idCategoria"];

				$respuesta = ModeloCategorias::mdlBorrarCategoria($tabla, $datos);

				if($respuesta == "ok"){
						echo '<script>

								swal({

									type: "success",
									title: "¡La categoría ha sido borrada correctamente!",
									showConfirmButton: true,
									confirmButtonText: "Cerrar"

								}).then(function(result){

									if(result.value){
						
										window.location = "categorias";

									}

								});
				

							  </script>';
				}

			}

		}
    }
    

?>