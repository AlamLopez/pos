<?php  
	
	/**
	 * Clase que gestiona el uso que se le da a la plantilla AdminLTE v2.4.2
	 * 
	 * @package controladores
	 * @author Alam Lopez <alam.lqnpa6@hotmail.com>
	 * 
	 */
	class ControladorPlantilla
	{
		/**
		 * Función que incluye el codigo de plantilla.php en la vista general del usuario
		 */
		static public function ctrPlantilla ()
		{
			include("vistas/plantilla.php");
		}
	}

?>