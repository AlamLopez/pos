<?php  
	
	/**
	 * Clase que gestiona los usuarios de la aplicación
	 * 
	 * @package controladores
	 * @author Alam Lopez <alam.lqnpa6@hotmail.com>
	 */
	class ControladorUsuarios
	{

		/*=============================================
		=            INGRESO DE USUARIOS            =
		=============================================*/
		
		/**
		 * Función que verifica el usuario y contraseña
		 * para hacer login a la aplicación (ingresar)
		 */
		static public function ctrIngresoUsuario()
		{
			// Se verifica que se reciben variables POST desde la vista del login.php
			if (isset($_POST["ingUsuario"])) {
				// Verifica que ingUsuario e ingPassword solo acepten caracteres alfanuméricos
				if(preg_match('/^[a-zA-Z0-9]+$/', $_POST["ingUsuario"]) && preg_match('/^[a-zA-Z0-9]+$/', $_POST["ingPassword"])){

					// Tabla de la BD en la que se verifica
					$tabla = "usuarios";

					// Encriptar la contraseña
					$encriptar = crypt($_POST["ingPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

					// Columna en la que se verifica
					$item = "usuario";

					// Valor que será verificado
					$valor = $_POST["ingUsuario"];

					// Solicita respuesta del modelo
					$respuesta = ModeloUsuarios::MdlMostrarUsuarios($tabla, $item, $valor);
					
					/* Si usuario coincide con lo almacenado en la BD, se redirige a inicio.php, si no muestra una alerta de error.*/
					if($respuesta["usuario"] == $_POST["ingUsuario"] && $respuesta["password"] == $encriptar){

						// Verifica que el usuario este Activo en el sistema
						if($respuesta["estado"] == 1){
							$_SESSION["iniciarSesion"] = "ok";
							$_SESSION["id"] = $respuesta["id"];
							$_SESSION["nombre"] = $respuesta["nombre"];
							$_SESSION["usuario"] = $respuesta["usuario"];
							$_SESSION["foto"] = $respuesta["foto"];
							$_SESSION["perfil"] = $respuesta["perfil"];

							// Registrar fecha y hora del ultimo login
							date_default_timezone_set('America/El_Salvador');

							$fecha = date('Y-m-d');
							$hora = date('H:i:s');

							$fechaActual = $fecha . ' ' . $hora;

							$item1 = "ultimo_login";
							$valor1 = $fechaActual;
							$item2 = "id";
							$valor2 = $respuesta["id"];

							$ultimoLogin = ModeloUsuarios::mdlActualizarUsuario($tabla, $item1, $valor1, $item2, $valor2);

							if($ultimoLogin == "ok"){
								echo '<script>
									window.location = "inicio";
								  </script>';
							}
						
						}else{
							echo '<br><div class="alert alert-danger" style="text-align:center;">Error al ingresar, el usuario aún no está activo</div>';
						}
						
						
					}else{
						echo '<br><div class="alert alert-danger" style="text-align:center;">Error al ingresar, vuelva a intentarlo</div>';
					}
				}
			}
		}


		/*=============================================
		=             CREACIÓN DE USUARIOS            =
		=============================================*/

		/**
		 * Función que recibe datos para ingresar un 
		 * nuevo usuario a la aplicación
		 */
		static public function ctrCrearUsuario(){

			// Se verifica que se recibe la variable POST desde la vista usuarios.php
			if(isset($_POST["nuevoUsuario"])){

				/* Verifica que nuevoNombre, nuevoUsuario y nuevoPassword solo acepten caracteres alfanuméricos */
				if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevoNombre"]) && 
				   preg_match('/^[a-zA-Z0-9]+$/', $_POST["nuevoUsuario"]) && 
				   preg_match('/^[a-zA-Z0-9]+$/', $_POST["nuevoPassword"])){


					$ruta = "";

					// Se verifica que se recibe una variable FILES desde la vista usuarios.php
					if(isset($_FILES["nuevaFoto"]["tmp_name"])){

						// Arreglo que almacena ancho y alto de imagen seleccionada
						list($ancho, $alto) = getimagesize($_FILES["nuevaFoto"]["tmp_name"]);

						$nuevoAncho = 500;
						$nuevoAlto = 500;

						// Se crea el directorio donde se guardaran las imágenes

						$directorio = "vistas/img/usuarios/" . $_POST["nuevoUsuario"];
						mkdir($directorio, 0755);

						// Si la imagen es JPG se aplican las funciones por defecto de PHP

						if($_FILES["nuevaFoto"]["type"] == "image/jpeg"){

							// Generar un número aleatorio para nombrar el archivo de imagen
							$aleatorio = mt_rand(100, 999);

							// Ruta del directorio donde se guardara la imagen
							$ruta = "vistas/img/usuarios/" . $_POST["nuevoUsuario"] . "/" . $aleatorio . ".jpg";

							// Se indica cual es el archivo de origen
							$origen = imagecreatefromjpeg($_FILES["nuevaFoto"]["tmp_name"]);

							// Se indica las nuevas propiedades que tendra la imagen
							$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

							// Se ajustan las imágenes a un formato estandar
							imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

							// Guardamos la imagen en el directorio
							imagejpeg($destino, $ruta);

						}

						// Si la imagen es PNG se aplican las funciones por defecto de PHP
						if($_FILES["nuevaFoto"]["type"] == "image/png"){

							// Generar un número aleatorio para nombrar el archivo de imagen
							$aleatorio = mt_rand(100, 999);

							// Ruta del directorio donde se guardara la imagen
							$ruta = "vistas/img/usuarios/" . $_POST["nuevoUsuario"] . "/" . $aleatorio . ".png";

							// Se indica cual es el archivo de origen
							$origen = imagecreatefrompng($_FILES["nuevaFoto"]["tmp_name"]);

							// Se indica las nuevas propiedades que tendra la imagen
							$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

							// Se ajustan las imágenes a un formato estandar
							imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

							// Guardamos la imagen en el directorio
							imagepng($destino, $ruta);

						}

					}

					// Tabla de la BD en la que se insertaran datos
					$tabla = "usuarios";

					// Encriptar la contraseña
					$encriptar = crypt($_POST["nuevoPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

					// Valores que serán insertados
					$datos = array('nombre' => $_POST["nuevoNombre"] , 
				                   'usuario' => $_POST["nuevoUsuario"] ,
				               	   'password' => $encriptar ,
				               	   'perfil' => $_POST["nuevoPerfil"],
				               	   'foto' => $ruta);

					// Solicita respuesta del modelo
					$respuesta = ModeloUsuarios::mdlIngresarUsuario($tabla, $datos);

					/* Si la respuesta es ok se muestra el SweetAlert de éxito, sino se muestra el SweetAlert de error*/
					
					if($respuesta == "ok"){
						echo '<script>

								swal({

									type: "success",
									title: "¡El usuario ha sido guardado correctamente!",
									showConfirmButton: true,
									confirmButtonText: "Cerrar"

								}).then(function(result){

									if(result.value){
						
										window.location = "usuarios";

									}

								});
				

							  </script>';
					}

				}else{

					echo '<script>

							swal({

								type: "error",
								title: "¡El usuario no puede ir vacío o llevar caracteres especiales!",
								showConfirmButton: true,
								confirmButtonText: "Cerrar"

							}).then(function(result){

								if(result.value){
						
									window.location = "usuarios";

								}

							});
				

						 </script>';

				}

			}

		}

		/*=============================================
		=            CONSULTA DE USUARIOS             =
		=============================================*/

		/**
		 * Función que recibe datos para ingresar un 
		 * nuevo usuario a la aplicación
		 */
		static public function ctrMostrarUsuarios($item, $valor){

			$tabla = "usuarios";
			$respuesta = ModeloUsuarios::MdlMostrarUsuarios($tabla, $item, $valor);
			return $respuesta;
		}


		/*=============================================
		=             EDICIÓN DE USUARIOS             =
		=============================================*/

		/**
		 * Función que recibe datos para editar las 
		 * propiedades de un usuario de la aplicación
		 */

		static public function ctrEditarUsuario(){

			if(isset($_POST["editarUsuario"])){
				
				/* Verifica que editarNombre*/
				if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["editarNombre"])){

					$ruta = $_POST["fotoActual"];

					// Se verifica que se recibe una variable FILES desde la vista usuarios.php
					if(isset($_FILES["editarFoto"]["tmp_name"]) && !empty($_FILES["editarFoto"]["tmp_name"])){

						// Arreglo que almacena ancho y alto de imagen seleccionada
						list($ancho, $alto) = getimagesize($_FILES["editarFoto"]["tmp_name"]);

						$nuevoAncho = 500;
						$nuevoAlto = 500;

						// Se crea el directorio donde se guardaran las imágenes
						$directorio = "vistas/img/usuarios/" . $_POST["editarUsuario"];

						// Se verifica si ya hay una foto guardada en la BD
						if(!empty($_POST["fotoActual"])){
							// Borra la carpeta de la foto actual
							unlink($_POST["fotoActual"]);
						}else{
							// Se crea la nueva carpeta con la foto
							mkdir($directorio, 0755);
						}

						// Si la imagen es JPG se aplican las funciones por defecto de PHP
						if($_FILES["editarFoto"]["type"] == "image/jpeg"){

							// Generar un número aleatorio para nombrar el archivo de imagen
							$aleatorio = mt_rand(100, 999);

							// Ruta del directorio donde se guardara la imagen
							$ruta = "vistas/img/usuarios/" . $_POST["editarUsuario"] . "/" . $aleatorio . ".jpg";

							// Se indica cual es el archivo de origen
							$origen = imagecreatefromjpeg($_FILES["editarFoto"]["tmp_name"]);

							// Se indica las nuevas propiedades que tendra la imagen
							$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

							// Se ajustan las imágenes a un formato estandar
							imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

							// Guardamos la imagen en el directorio
							imagejpeg($destino, $ruta);

						}

						// Si la imagen es PNG se aplican las funciones por defecto de PHP
						if($_FILES["editarFoto"]["type"] == "image/png"){

							// Generar un número aleatorio para nombrar el archivo de imagen
							$aleatorio = mt_rand(100, 999);

							// Ruta del directorio donde se guardara la imagen
							$ruta = "vistas/img/usuarios/" . $_POST["editarUsuario"] . "/" . $aleatorio . ".png";

							// Se indica cual es el archivo de origen
							$origen = imagecreatefrompng($_FILES["editarFoto"]["tmp_name"]);

							// Se indica las nuevas propiedades que tendra la imagen
							$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

							// Se ajustan las imágenes a un formato estandar
							imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

							// Guardamos la imagen en el directorio
							imagepng($destino, $ruta);

						}

					}

					// Tabla de la BD en la que se editarán datos
					$tabla = "usuarios";

					// Se verifica si la contraseña será cambiada
					if($_POST["editarPassword"] != ""){
						
						if(preg_match('/^[a-zA-Z0-9]+$/', $_POST["editarPassword"])){
							$encriptar = crypt($_POST["editarPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
						}else{
							echo '<script>

									swal({

										type: "error",
										title: "¡La contraseña llevar caracteres especiales!",
										showConfirmButton: true,
										confirmButtonText: "Cerrar"

									}).then(function(result){

										if(result.value){
						
											window.location = "usuarios";

										}

									});
				

						 		</script>';
						}

					}else{
						$encriptar = $_POST["passwordActual"];
					}

					// Valor que serán editados
					$datos = array('nombre' => $_POST["editarNombre"] , 
				                   'usuario' => $_POST["editarUsuario"] ,
				               	   'password' => $encriptar,
				               	   'perfil' => $_POST["editarPerfil"],
				               	   'foto' => $ruta);

					// Solicita respuesta del modelo
					$respuesta = ModeloUsuarios::mdlEditarUsuario($tabla, $datos);

					/* Si la respuesta es ok se muestra el SweetAlert de éxito, sino se muestra el SweetAlert de error*/
					if($respuesta == "ok"){
						echo '<script>

								swal({

									type: "success",
									title: "¡El usuario ha sido editado correctamente!",
									showConfirmButton: true,
									confirmButtonText: "Cerrar"

								}).then(function(result){

									if(result.value){
						
										window.location = "usuarios";

									}

								});
				

							  </script>';
					}


				}else{
				echo '<script>

						swal({

							type: "error",
							title: "¡El nombre de usuario no puede ir vacío o llevar caracteres especiales!",
							showConfirmButton: true,
							confirmButtonText: "Cerrar"

						}).then(function(result){

							if(result.value){
						
								window.location = "usuarios";

							}

						});
				

					  </script>';
				}


			}

		}

		/*=============================================
		=          ELIMINACIÓN DE USUARIOS            =
		=============================================*/

		/**
		 * Función que recibe datos para eliminar  
		 * a un usuario de la aplicación segun su ID
		 */

		static public function ctrBorrarUsuario(){

			if(isset($_GET["idUsuario"])){

				$tabla = "usuarios";
				$datos = $_GET["idUsuario"];

				if($_GET["fotoUsuario"] != ""){

					unlink($_GET["fotoUsuario"]);
					rmdir('vistas/img/usuarios/' . $_GET["usuario"]);
				}

				$respuesta = ModeloUsuarios::mdlBorrarUsuario($tabla, $datos);

				if($respuesta == "ok"){
						echo '<script>

								swal({

									type: "success",
									title: "¡El usuario ha sido borrado correctamente!",
									showConfirmButton: true,
									confirmButtonText: "Cerrar"

								}).then(function(result){

									if(result.value){
						
										window.location = "usuarios";

									}

								});
				

							  </script>';
				}

			}

		}


	}
	

?>