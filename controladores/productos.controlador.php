<?php  
    
    class ControladorProductos{

        /*=============================================
		=            CONSULTA DE PRODUCTOS             =
		=============================================*/

		/**
		 * Función que recibe datos para mostrar 
		 * los productos de la aplicación
		 */
		static public function ctrMostrarProductos($item, $valor){

			$tabla = "productos";
			$respuesta = ModeloProductos::MdlMostrarProductos($tabla, $item, $valor);
			return $respuesta;
		}

		/*=============================================
		=              CREAR PRODUCTOS                =
		=============================================*/

		/**
		 * Función que recibe datos para ingresar un 
		 * nuevo producto a la aplicación
		 */
		static public function ctrCrearProducto(){

			if(isset($_POST["nuevaDescripcion"])){
				
				if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevaDescripcion"]) && preg_match('/^[0-9]+$/', $_POST["nuevoStock"]) && preg_match('/^[0-9.]+$/', $_POST["nuevoPrecioCompra"]) && preg_match('/^[0-9.]+$/', $_POST["nuevoPrecioVenta"])){

					$ruta = "vistas/img/productos/default/anonymous.png";

					// Se verifica que se recibe una variable FILES desde la vista usuarios.php
					if(isset($_FILES["nuevaImagen"]["tmp_name"])){

						// Arreglo que almacena ancho y alto de imagen seleccionada
						list($ancho, $alto) = getimagesize($_FILES["nuevaImagen"]["tmp_name"]);

						$nuevoAncho = 500;
						$nuevoAlto = 500;

						// Se crea el directorio donde se guardaran las imágenes

						$directorio = "vistas/img/productos/" . $_POST["nuevoCodigo"];
						mkdir($directorio, 0755);

						// Si la imagen es JPG se aplican las funciones por defecto de PHP

						if($_FILES["nuevaImagen"]["type"] == "image/jpeg"){

							// Generar un número aleatorio para nombrar el archivo de imagen
							$aleatorio = mt_rand(100, 999);

							// Ruta del directorio donde se guardara la imagen
							$ruta = "vistas/img/productos/" . $_POST["nuevoCodigo"] . "/" . $aleatorio . ".jpg";

							// Se indica cual es el archivo de origen
							$origen = imagecreatefromjpeg($_FILES["nuevaImagen"]["tmp_name"]);

							// Se indica las nuevas propiedades que tendra la imagen
							$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

							// Se ajustan las imágenes a un formato estandar
							imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

							// Guardamos la imagen en el directorio
							imagejpeg($destino, $ruta);

						}

						// Si la imagen es PNG se aplican las funciones por defecto de PHP
						if($_FILES["nuevaImagen"]["type"] == "image/png"){

							// Generar un número aleatorio para nombrar el archivo de imagen
							$aleatorio = mt_rand(100, 999);

							// Ruta del directorio donde se guardara la imagen
							$ruta = "vistas/img/productos/" . $_POST["nuevoCodigo"] . "/" . $aleatorio . ".png";

							// Se indica cual es el archivo de origen
							$origen = imagecreatefrompng($_FILES["nuevaImagen"]["tmp_name"]);

							// Se indica las nuevas propiedades que tendra la imagen
							$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

							// Se ajustan las imágenes a un formato estandar
							imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

							// Guardamos la imagen en el directorio
							imagepng($destino, $ruta);

						}

					}
					
					$tabla = "productos";
					$datos = array("id_categoria" => $_POST["nuevaCategoria"],
							   		"codigo" => $_POST["nuevoCodigo"],
							   		"descripcion" => $_POST["nuevaDescripcion"],
							   		"stock" => $_POST["nuevoStock"],
							   		"precio_compra" => $_POST["nuevoPrecioCompra"],
							   		"precio_venta" => $_POST["nuevoPrecioVenta"],
									"imagen" => $ruta);
					
					$respuesta = ModeloProductos::mdlIngresarProducto($tabla, $datos);

					if($respuesta == "ok"){

						echo'<script>
	
							swal({
								  type: "success",
								  title: "El producto ha sido guardado correctamente",
								  showConfirmButton: true,
								  confirmButtonText: "Cerrar"
								  }).then(function(result){
											if (result.value) {
	
											window.location = "productos";
	
											}
										})
	
							</script>';
	
					}

				}else{
					echo'<script>

					swal({
						  type: "error",
						  title: "¡El producto no puede ir con los campos vacíos o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "productos";

							}
						})

			  	</script>';
				}
			}

		} 

		/*=============================================
		=              EDITAR PRODUCTOS               =
		=============================================*/

		/**
		 * Función que recibe datos para ingresar un 
		 * nuevo producto a la aplicación
		 */
		static public function ctrEditarProducto(){

			if(isset($_POST["editarDescripcion"])){
				
				if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["editarDescripcion"]) && preg_match('/^[0-9]+$/', $_POST["editarStock"]) && preg_match('/^[0-9.]+$/', $_POST["editarPrecioCompra"]) && preg_match('/^[0-9.]+$/', $_POST["editarPrecioVenta"])){

					$ruta = $_POST["imagenActual"];

					// Se verifica que se recibe una variable FILES desde la vista usuarios.php
					if(isset($_FILES["editarImagen"]["tmp_name"]) && !empty($_FILES["editarImagen"]["tmp_name"])){

						// Arreglo que almacena ancho y alto de imagen seleccionada
						list($ancho, $alto) = getimagesize($_FILES["editarImagen"]["tmp_name"]);

						$nuevoAncho = 500;
						$nuevoAlto = 500;

						// Se crea el directorio donde se guardaran las imágenes
						$directorio = "vistas/img/productos/" . $_POST["editarCodigo"];

						//Se verifica si ya existe una imagen en la base de datos
						if (!empty($_POST["imagenActual"]) && $_POST["imagenActual"] != "vistas/img/productos/default/anonymous.png") {
							unlink($_POST["imagenActual"]);
						}else{
							mkdir($directorio, 0755);
						}

						// Si la imagen es JPG se aplican las funciones por defecto de PHP
						if($_FILES["editarImagen"]["type"] == "image/jpeg"){

							// Generar un número aleatorio para nombrar el archivo de imagen
							$aleatorio = mt_rand(100, 999);

							// Ruta del directorio donde se guardara la imagen
							$ruta = "vistas/img/productos/" . $_POST["editarCodigo"] . "/" . $aleatorio . ".jpg";

							// Se indica cual es el archivo de origen
							$origen = imagecreatefromjpeg($_FILES["editarImagen"]["tmp_name"]);

							// Se indica las nuevas propiedades que tendra la imagen
							$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

							// Se ajustan las imágenes a un formato estandar
							imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

							// Guardamos la imagen en el directorio
							imagejpeg($destino, $ruta);

						}

						// Si la imagen es PNG se aplican las funciones por defecto de PHP
						if($_FILES["editarImagen"]["type"] == "image/png"){

							// Generar un número aleatorio para nombrar el archivo de imagen
							$aleatorio = mt_rand(100, 999);

							// Ruta del directorio donde se guardara la imagen
							$ruta = "vistas/img/productos/" . $_POST["editarCodigo"] . "/" . $aleatorio . ".png";

							// Se indica cual es el archivo de origen
							$origen = imagecreatefrompng($_FILES["editarImagen"]["tmp_name"]);

							// Se indica las nuevas propiedades que tendra la imagen
							$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

							// Se ajustan las imágenes a un formato estandar
							imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

							// Guardamos la imagen en el directorio
							imagepng($destino, $ruta);

						}

					}
					
					$tabla = "productos";
					$datos = array("id_categoria" => $_POST["editarCategoria"],
							   		"codigo" => $_POST["editarCodigo"],
							   		"descripcion" => $_POST["editarDescripcion"],
							   		"stock" => $_POST["editarStock"],
							   		"precio_compra" => $_POST["editarPrecioCompra"],
							   		"precio_venta" => $_POST["editarPrecioVenta"],
									"imagen" => $ruta);
					
					$respuesta = ModeloProductos::mdlEditarProducto($tabla, $datos);

					if($respuesta == "ok"){

						echo'<script>
	
							swal({
								  type: "success",
								  title: "El producto ha sido editado correctamente",
								  showConfirmButton: true,
								  confirmButtonText: "Cerrar"
								  }).then(function(result){
											if (result.value) {
	
											window.location = "productos";
	
											}
										})
	
							</script>';
	
					}

				}else{
					echo'<script>

					swal({
						  type: "error",
						  title: "¡El producto no puede ir con los campos vacíos o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "productos";

							}
						})

			  	</script>';
				}
			}

		}

		/*=============================================
		=             ELIMINAR PRODUCTOS              =
		=============================================*/

		static public function ctrEliminarProducto(){

			if(isset($_GET["idProducto"])){

				$tabla = "productos";
				$datos = $_GET["idProducto"];

				if($_GET["imagen"] != "" && $_GET["imagen"] != "vistas/img/productos/default/anonymous.png"){

					unlink($_GET["imagen"]);
					rmdir("vistas/img/productos/" . $_GET["codigo"]);

				}

				$respuesta = ModeloProductos::mdlEliminarProducto($tabla, $datos);

				if($respuesta == "ok"){
					echo '<script>

							swal({

								type: "success",
								title: "¡El producto ha sido borrado correctamente!",
								showConfirmButton: true,
								confirmButtonText: "Cerrar"

							}).then(function(result){

								if(result.value){
					
									window.location = "productos";

								}

							});
			

						  </script>';
				}
			}
		}

    }
    
	

?>