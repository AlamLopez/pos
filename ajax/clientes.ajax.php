<?php
	
	require_once "../controladores/clientes.controlador.php";
    require_once "../modelos/clientes.modelo.php";
    
    /**
	 * Clase Ajax que consulta en la BD los clientes y 
	 * los devuelve en formato JSON para mostrarlos en 
	 * las entradas del formulario de edición.
	 * 
	 * @package ajax
	 * @author Alam Lopez <alam.lqnpa6@hotmail.com>
	 */
    class AjaxClientes
    {

        public $idCliente;

        /*=============================================
		=               EDITAR CLIENTES               =
		=============================================*/

		/**
		 * Función que consulta clientes por ID y 
		 * devuelve el resultado de la consulta en JSON
		 */
		public function ajaxEditarCliente(){
			
			$item = "id";
			
			$valor = $this -> idCliente;
			
			// Se envian parámetos para consultar en la BD
			$respuesta = ControladorClientes::ctrMostrarClientes($item, $valor);

			// El resultado se codifca en formato JSON
			echo json_encode($respuesta);
        }
        
    }

    /*=============================================
	=               EDITAR CLIENTES             =
	=============================================*/

	// Se verifica que existe el idCliente
	if(isset($_POST["idCliente"])){

		// Instancia de la clase AjaxClientes
		$editar = new AjaxClientes();
		
		// Se le asigna valor a la propiedad idCliente
		$editar -> idCliente = $_POST["idCliente"];

		// Se invoca la funcion
		$editar -> ajaxEditarCliente();
	}


?>