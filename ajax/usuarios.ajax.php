<?php
	
	require_once "../controladores/usuarios.controlador.php";
	require_once "../modelos/usuarios.modelo.php";

	/**
	 * Clase Ajax que consulta en la BD los usuarios y 
	 * los devuelve en formato JSON para mostrarlos en 
	 * las entradas del formulario de edición.
	 * 
	 * @package ajax
	 * @author Alam Lopez <alam.lqnpa6@hotmail.com>
	 */
	class AjaxUsuarios
	{

		// Propiedad de la clase AjaxUsuarios
		public $idUsuario;


		/*=============================================
		=               EDITAR USUARIOS               =
		=============================================*/

		/**
		 * Función que consulta usuarios por ID y 
		 * devuelve el resultado de la consulta en JSON
		 */
		public function ajaxEditarUsuario(){
			
			$item = "id";
			
			$valor = $this -> idUsuario;
			
			// Se envian parámetos para consultar en la BD
			$respuesta = ControladorUsuarios::ctrMostrarUsuarios($item, $valor);

			// El resultado se codifca en formato JSON
			echo json_encode($respuesta);
		}

		// Propiedades de la clase AjaxUsuarios
		public $activarUsuario;
		public $activarId;

		/*=============================================
		=              ACTIVAR USUARIOS               =
		=============================================*/

		/**
		 * Función que actualiza el estado de un usuarios  
		 * (Activado/Desactivado) según su ID
		 */
		public function ajaxActivarUsuario(){

			$tabla = "usuarios";
			$item1 = "estado";
			$valor1 = $this -> activarUsuario;

			$item2 = "id";
			$valor2 = $this -> activarId;
			$respuesta = ModeloUsuarios::mdlActualizarUsuario($tabla, $item1, $valor1, $item2, $valor2);
		}

		/*=============================================
		=        VALIDAR NO REPETIR USUARIOS          =
		=============================================*/

		public $validarUsuario;

		public function ajaxValidarUsuario(){
			
			$item = "usuario";
			
			$valor = $this -> validarUsuario;
			
			// Se envian parámetos para consultar en la BD
			$respuesta = ControladorUsuarios::ctrMostrarUsuarios($item, $valor);

			// El resultado se codifca en formato JSON
			echo json_encode($respuesta);
		}

	}

	/*=============================================
	=               EDITAR USUARIOS               =
	=============================================*/

	// Se verifica que existe el idUsuario
	if(isset($_POST["idUsuario"])){

		// Instancia de la clase AjaxUsuario
		$editar = new AjaxUsuarios();
		
		// Se le asigna valor a la propiedad idUsuario
		$editar -> idUsuario = $_POST["idUsuario"];

		// Se invoca la funcion
		$editar -> ajaxEditarUsuario();
	}

	/*=============================================
	=              ACTIVAR USUARIOS               =
	=============================================*/

	if(isset($_POST["activarUsuario"])){
		$activarUsuario = new AjaxUsuarios();
		$activarUsuario -> activarUsuario = $_POST["activarUsuario"];
		$activarUsuario -> activarId = $_POST["activarId"];
		$activarUsuario -> ajaxActivarUsuario();

	}

	/*=============================================
		=        VALIDAR NO REPETIR USUARIOS          =
		=============================================*/
	if (isset($_POST["validarUsuario"])) {
		
		$valUsuario = new AjaxUsuarios();
		$valUsuario -> validarUsuario = $_POST["validarUsuario"];
		$valUsuario -> ajaxValidarUsuario();
	}


?>