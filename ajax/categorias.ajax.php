<?php
	
	require_once "../controladores/categorias.controlador.php";
	require_once "../modelos/categorias.modelo.php";

	/**
	 * Clase Ajax que consulta en la BD las categorias y 
	 * los devuelve en formato JSON para mostrarlos en 
	 * las entradas del formulario de edición.
	 * 
	 * @package ajax
	 * @author Alam Lopez <alam.lqnpa6@hotmail.com>
	 */
	class AjaxCategorias
	{

		// Propiedad de la clase AjaxCategorias
		public $idCategoria;

		/*=============================================
		=        VALIDAR NO REPETIR CATEGORÍA          =
		=============================================*/

		public $validarCategoria;

		public function ajaxValidarCategoria(){
			
			$item = "categoria";
			
			$valor = $this -> validarCategoria;
			
			// Se envian parámetos para consultar en la BD
			$respuesta = ControladorCategorias::ctrMostrarCategorias($item, $valor);

			// El resultado se codifca en formato JSON
			echo json_encode($respuesta);
		}

		/*=============================================
		=               EDITAR CATEGORIAS               =
		=============================================*/

		/**
		 * Función que consulta categorias por ID y 
		 * devuelve el resultado de la consulta en JSON
		 */
		public function ajaxEditarCategoria(){
			
			$item = "id";
			
			$valor = $this -> idCategoria;
			
			// Se envian parámetos para consultar en la BD
			$respuesta = ControladorCategorias::ctrMostrarCategorias($item, $valor);

			// El resultado se codifca en formato JSON
			echo json_encode($respuesta);
		}

	}

	/*=============================================
		=        VALIDAR NO REPETIR CATEGORÍA          =
		=============================================*/
	if (isset($_POST["validarCategoria"])) {
		
		$valCategoria = new AjaxCategorias();
		$valCategoria -> validarCategoria = $_POST["validarCategoria"];
		$valCategoria -> ajaxValidarCategoria();
	}

	/*=============================================
	=               EDITAR CATEGORÍAS             =
	=============================================*/

	// Se verifica que existe el idCategoria
	if(isset($_POST["idCategoria"])){

		// Instancia de la clase AjaxCategorias
		$editar = new AjaxCategorias();
		
		// Se le asigna valor a la propiedad idCategoria
		$editar -> idCategoria = $_POST["idCategoria"];

		// Se invoca la funcion
		$editar -> ajaxEditarCategoria();
	}


?>